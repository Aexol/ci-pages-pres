# YAML tricks

---
### Stringi multiline
* '|' rozpoczyna blok wieloliniowego stringa *zachowującego* znaki nowej lini
* '>' rozpoczyna blok wieloliniowego stringa *zamieniającego* znaki nowej lini na spacje
```yaml
przyklad1: |
to jest
dlugi napis zachowujący znaki nowej lini
przyklad2: >
to jest
dlugi napis zamieniajacy znaki nowej lini na spacje
```


---
### Anchor, Reference, Extend

* YAML pozwala na zdefiniowanie tzw `anchor'a` używając znaku '&'
* Anchor'a można użyć jako referencji lub rozszerzyć istniejący obiekt:
* Gitlab ignoruje klucze w <span class="inline-hh">.gitlab-ci.yml</span> zaczynające się od kropki. Można tego użyć do utrzymania CI 'DRY'

```yaml
.before_script: &before_script_template #anchor
- echo "hey!"
before_script: *before_script_template #reference
job1:
  <<: *before_script_template #extend
  script:
  - echo "ho!"
```