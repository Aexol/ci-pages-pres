### Wprowadzenie do Gitlab CI/CD

---
### Jak to działa?

Gitlab CI/CD jest zintegrowany razem całym środowiskiem Gitlab'a.

---
### Po co?

Bo jest to modne. A tak serio, automatyzacja budowania, testów i deploymentu.

---
### Dlaczego akurat Gitlab CI/CD?

Konkurencji jest sporo. Dlaczego akurat Gitlab CI/CD? Jednym z głównych atutów według mnie
jest właśnie głęboka integracja Gitlab CI/CD z całym środowiskiem Gitlab'a.
