## Wreszcie ... Gitlab Pages

---
### Co to jest?

* Automatyczny deploy statycznych danych strony/aplikacji na hosting skonfigurowany w serwerze gitlab'a
* Specjalny <span class="inline-hh">job</span> zdefiniowany w w pliku <span class="inline-hh">.gitlab-ci.yml</span>
* W przypadku gitlab.com, adres twojej aplikacji/strony będzie to <span class="inline-hh">namespace.gitlab.io/nazwa-projektu</span>

---
### Domena

* Możliwość konfiguracji własnej domeny

---
### SSL/TLS? Czyli czy to boli?

* Integracja z Let's Encrypt
* Możliwość wrzucenia własnego certyfikatu (pozwala na integrację np z Cloudflare)

---
### Job pages

* Aby wrzucić statyczne dane na serwer w pliku <span class="inline-hh">.gitlab-ci.yml</span> musi być zdefiniowany <span class="inline-hh">job</span> <span class="inline-hh">pages</span>
```yaml
pages:
  stage: deploy
  script:
    - mkdir .public
    - cp -r * .public
    - mv .public public
  artifacts:
    paths:
      - public
  only:
    - master
```

---
### CI tej prezentacji

```yaml
image: "node:10-slim"

stages:
- build
- pages

build:
  stage: build
  script:
  - wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
  - sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
  - apt-get update
  - apt-get install -y google-chrome-unstable fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst fonts-freefont-ttf --no-install-recommends
  - npm install --only=dev
  - npm install --global grunt-cli
  - grunt
  - mkdir public
  - mv {content,css,js,lib,plugin}/ index.html pdf.css public/
  artifacts:
    name: public
    expire_in: 31d
    paths:
    - public/

pages:
  stage: pages
  dependencies:
    - build
  script:
  - echo "noop"
  artifacts:
    paths:
      - public/
  only:
    - master
```