### Stage i Job

Podstawowymi jednostkami w Gitlab CI są <span class="inline-hh">stage</span> i <span class="inline-hh">job</span>

---
### Stage

* Jeden z przystanków w całym <span class="inline-hh">pipeline</span>'ie CI/CD.
* <span class="inline-hh">Stage</span> może zawierać zero lub więcej nie zależnych <span class="inline-hh">job</span>

---
### Domyślne stage

* Domyślnie zadeklarowane są trzy <span class="inline-hh">stage</span>: <span class="inline-hh">build</span>, <span class="inline-hh">test</span>, <span class="inline-hh">deploy</span>
* Stage wykonywane są w kolejności deklaracji
* Przykład <span class="inline-hh">pipeline</span>'a z trzema <span class="inline-hh">stage</span>'ami <span class="inline-hh">build</span>, <span class="inline-hh">test</span> i <span class="inline-hh">publish</span> zadeklarowanymi przez użytkownika:

```yaml
stages:
- build
- test
- publish
```

---
### Job

Podstawowy element w twoim <span class="inline-hh">pipeline</span>.