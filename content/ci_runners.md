# CI Runners

---
### Shared, specific, group runners

* Shared - Współdzielone runner'y dla podobnych workload'ów. Gitlab.com dostarcza nam współdzielone runner do użytku z limitami.
* Specific - dla <span class="inline-hh">job</span>'ów które mają specjalne wymagania
* Group - przydzielone do grupy w gitlab'ie
* Możliwość podłączenia własnych (bardzo proste!!!) runner'ów nawet na gitlab.com

---
### Aktualnie wspierane runner'y
* Shell
* Docker
* Docker Machine i Docker Machine SSH
* Parallels
* VirtualBox
* SSH
* Kubernetes