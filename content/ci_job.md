## Ciekawsze opcje konfiguracji job

---
### script

* <span class="inline-hh">String</span> lub lista <span class="inline-hh">string</span>.
* W sumie to co twój <span class="inline-hh">job</span> aktualnie robi

```
job1:
  script: |-
    echo "hello world"

job2:
  script:
  - echo "hello world"
  - echo "goodbye"
```

---
### before_script

* <span class="inline-hh">String</span> lub lista <span class="inline-hh">string</span>.
* Może być ustawiony globalnie lub per-<span class="inline-hh">job</span>. Ustawienie per-<span class="inline-hh">job</span> nadpisuje ustawienie globalne.
* Przydatne przy współdzielonych setup'ach środowiska.

```
before_script:
- mkdir -p ~/.docker
- echo "${DOCKER_CONFIG}" > ~/.docker/config.json
job1:
    before_script:
    - mkdir -p ~/.docker
    - echo "${DOCKER_CONFIG_ALT}" > ~/.docker/config.json
```

---
### stage

<span class="inline-hh">String</span> określający do którego <span class="inline-hh">stage</span>'a należy <span class="inline-hh">job</span> (domyślnie: test).

```yaml
job1:
  stage: build
```

---
### cache

* Pozwala na przekazywanie danych między stage'ami.
* Przydatne w przyśpieszaniu CI/CD poprzez przekazywanie np zależności (<span class="inline-hh">node_modules</span>, <span class="inline-hh">GOPATH</span>, <span class="inline-hh">virtualnenv</span> etc.)
* Może też być zadeklarowany globalnie. Lokalna deklaracja nadpisuje globalną

```
cache:
  paths:
  - node_modules
```

---
### artifacts

* Deklaruje listę załączonych do <span class="inline-hh">job</span>'a.
* Pliki są dostępne do ściągnięcia oraz przekazane do następnych <span class="inline-hh">stage</span>'y <span class="inline-hh">pipeline</span>'a.

```
artifacts:
  paths:
  - binaries/
```

---
### image

* Wskazuje jakiego obrazu docker'owego użyć w <span class="inline-hh">job</span>'ie.
* Może być zadeklarowany globalnie dla całego <span class="inline-hh">pipeline</span>'a.
* Lokalna deklaracje nadpisuje globalną.

```
image: node:8

job1:
    image: busybox:latest
    script: |-
        echo "hello world"
```

---
### variables

* Mogą być ustawione globalnie i dla każdego <span class="inline-hh">job</span>'a. Deklaracja w <span class="inline-hh">job</span>'ie ma priorytet
* Ustawia zmienną środowiskową dla <span class="inline-hh">job</span>'a
* Zmienne środowiskowe mogą być ustawione w wielu innych miejscach. [Więcej informacji w dokumentacji GitLaba](https://docs.gitlab.com/ee/ci/variables/README.html#priority-of-environment-variables)

```yaml
variables:
  DB_URL: url.do.bazy.danych
job1:
  variables:
    DB_URL: inny.url.do.bazy.danych
```