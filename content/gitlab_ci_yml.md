## .gitlab-ci.yml

---

### Co to jest?

Pliczek znajdujący się w twoim repozytorium git'a. Jest to poprostu konfiguracja twojego CI/CD.

Serwer GitLab'a automatycznie powinien go wykryć i uruchomić twój <span class="inline-hh">pipeline</span>.

---

### Prosty przykład

```yaml
build:
  stage: build
  script:
  - npm i
  - npm run build

test:
  stage: test
  script:
  - npm i
  - npm run build
  - npm run test

deploy:
  stage: deploy
  script:
  - npm i
  - npm run build
  - npm publish
```